-- liquibase formatted sql changeLogId:91906abf-36e7-47f3-96a2-9768d6ff60cb

-- changeset SteveZ:createTable_salesTableZ
CREATE TABLE salesTableZ (
    ID int NOT NULL,
    NAME varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    REGION varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    MARKET varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
--rollback DROP TABLE salesTableZ
